pipeline {
  
  agent any

  environment {
    deploymentName = "devsecops"
    containerName = "devsecops-container"
    serviceName = "devsecops-svc"
    imageName = "pprasant/devsecops:${BUILD_ID}"
    applicationURL = "http://18.153.90.217"
    applicationURI = "/increment/99"
  }

  stages {
      stage('Build Artifact') {
            steps {
              sh 'mvn clean package -DskipTests=true'
              archive 'target/*.jar'
            }
        }   

      stage('Unit Tests - JUnit and Jacoco') {
        steps {
          sh 'mvn test'
        }
      }

      stage('Mutation Tests - PIT') {
        steps {
          sh 'mvn org.pitest:pitest-maven:mutationCoverage'
        }
      }

      stage('SonarQube Scan - SAST') {
        steps {
          withSonarQubeEnv('SonarQube') {
          sh "mvn sonar:sonar -Dsonar.projectKey=dev-sec-ops -Dsonar.host.url=http://18.153.90.217:9000 -Dsonar.login=sqp_525212b52651d1396e77c7df2b9a5801cbda9c83"
        }
        timeout(time: 2, unit: 'MINUTES') {
          script {
            waitForQualityGate abortPipeline: true
          }
        }
        }
    
      }


      stage('Vulnerability Scan - Docker') {
        steps {
          parallel(
            "Dependency Scan": {
              sh "mvn dependency-check:check"
            },
            "Trivy Scan": {
              sh "bash trivy-docker-image-scan.sh"
            },
            "OPA Conftest": {
              sh 'docker run --rm -v $(pwd):/project openpolicyagent/conftest test --policy opa-docker-security.rego Dockerfile'
            }
          )
        }
      }

      stage('Docker build and Push') {
        steps {

          withDockerRegistry([credentialsId: "dockerhub-credential", url: ""]) {
            sh 'printenv'
            sh "sudo docker build -t pprasant/devsecops:${BUILD_ID} ."
            sh "docker push pprasant/devsecops:${BUILD_ID}"

          }

        }

      }
      

      stage('Vulnerability Scan - Kubernetes') {
        steps {
          parallel(
            "OPA Scan": {
              sh 'docker run --rm -v $(pwd):/project openpolicyagent/conftest test --policy opa-k8s-security.rego k8s_deployment_service.yaml'
            },
            "Kubesec Scan": {
              sh "bash kubesec-scan.sh"
            },
            "Trivy Scan": {
              sh "bash trivy-k8s-scan.sh"
            }
          )
        }
      }


      stage('Kubernetes Deployment - DEV') {
        steps {
          parallel(
            "Deployment": {
              withKubeConfig([credentialsId: 'Kubeconfig']) {
                sh "bash k8s-deployment.sh"
              }
            },
            "Rollout Status": {
              withKubeConfig([credentialsId: 'Kubeconfig']) {
                sh "bash k8s-deployment-rollout-status.sh"
              }
            }
          )
        }
      }

      stage('Integration Tests - DEV') {
        steps {
          script {
            try {
              withKubeConfig([credentialsId: 'Kubeconfig']) {
                sh "bash integration-test.sh"
              }
            }

            catch (e) {
              withKubeConfig([credentialsId: 'Kubeconfig']) {
                sh "kubectl -n default rollout undo deploy ${deploymentName}"
              }

              throw e

            }
          }
        }
      }

      stage('OWASP ZAP - DAST') {
        steps {
          withKubeConfig([credentialsId: 'Kubeconfig']) {
            sh "bash zap.sh"
          }
        }
      }

      stage('Prompt to PROD?') {
        steps {
          timeout(time: 2, unit: 'DAYS') {
            input 'Do you want to Approve the Deployment to Production Environment/Namespace?'
          }
        }
      }

      stage('K8S CIS Benchmark') {
        steps {
          script {
            parallel(
              "Master": {
                sh "bash cis-master.sh"
              },
              "Etcd": {
                sh "bash cis-etcd.sh"
              },
              "Kubelet": {
                sh "bash cis-kubelet.sh"
              }
            )
          }
        }
      }

      stage('K8S Deployment - PROD') {
        steps {
          parallel(
            "Deployment": {
              withKubeConfig([credentialsId:'Kubeconfig']) {
                sh "sed -i 's#replace#${imageName}#g' k8s_prod_deployment_service.yaml"
                sh "kubectl -n prod apply -f k8s_prod_deployment_service.yaml"
              }
            },
            "Rollout Status": {
              withKubeConfig([credentialsId: 'Kubeconfig']) {
                sh "bash k8s_prod_deployment_rollout_status.sh"
              }
            }
          )
        }
      }

      stage('Integration Tests - PROD') {
        steps {
          script {
            try {
              withKubeConfig([credentialsId: 'Kubeconfig']) {
                sh "bash integration-test-prod.sh"
              }
            }
            catch (e) {
              withKubeConfig([credentialsId: 'Kubeconfig']) {
                sh "kubectl -n prod rollout undo deploy ${deploymentName}"
              }
              throw e
            }
          }
        }
      }


    }

    post {
      always {
            junit 'target/surefire-reports/*.xml'
            jacoco execPattern: 'target/jacoco.exec'
            pitmutation mutationStatsFile: '**/target/pit-reports/**/mutations.xml'
            dependencyCheckPublisher pattern: 'target/dependency-check-report.xml'
            publishHTML([allowMissing: false, alwaysLinkToLastBuild: true, keepAll: true, reportDir: 'owasp-zap-report', reportFiles: 'zap_report.html', reportName: 'HTML Report', reportTitles: 'OWASP_ZAP_HTML', useWrapperFileDirectly: true])
      }
    }
}

